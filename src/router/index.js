import Vue from "vue";
import VueRouter from "vue-router";

Vue.use(VueRouter);

export default new VueRouter({
    mode: "history",
    routes: [
        { path: "*", redirect: "/" },
        {
            path: '/', name:'home',
            meta: { header: "home", footer: 'home' },
            component: () => import('../views/Home.vue')
        },
        {
            path: '/login', name:'login',
            component: () => import('../views/Login.vue')
        },
        {
            path: '/register', name:'register',
            component: () => import('../views/Register.vue')
        },
        {
            path: '/travels', name:'travels',
            component: () => import('../views/Travels.vue')
        },
        {
            path: '/travel/:id', name:'travel',
            component: () => import('../views/Travel.vue')
        },
        {
            path: '/travel/new', name:'travel-new',
            component: () => import('../views/CreateTravel.vue')
        },
        {
            path: '/uploadtravel', name:'uploadtravel',
            component: () => import('../views/CreateTravel.vue')
        },
        {
            path: '/edittravel', name:'edittravel',
            component: () => import('../views/UpdateTravel.vue')
        },
    ]
});
