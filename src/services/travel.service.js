import axios from 'axios';

const API_URL = 'http://travel.local/api/travel';

class TravelService {
    getAll() {
        return axios.get(API_URL);
    }

    getTravel(id) {
        return axios.get(`${API_URL}${id}`)
    }

    setTravel(form) {
        return axios.post(`${API_URL}`, {
            name: form.title,
            description: form.description,
            author: `/api/users/1`
        })
    }
}

export default new TravelService();