import axios from 'axios';

const API_URL = 'http://travel.local/api/';

class AuthService {
    login(form) {
        return axios
            .post(API_URL + 'login_check', {
                username: form.username,
                password: form.password,
                remember_me: true
            })
            .then(response => {
                if (response && response.data && response.data.token) {
                    localStorage.setItem('user', JSON.stringify(response.data));

                    return response.data;
                }
            });
    }

    logout() {
        localStorage.removeItem('user');
    }

    register(user) {
        return axios.post(API_URL + 'register', {
            username: user.username,
            email: user.email,
            password: user.password
        });
    }
}

export default new AuthService();