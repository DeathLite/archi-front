import '@fortawesome/fontawesome-free/css/all.min.css'
import Vue from 'vue';
import router from "./router";
import store from './store';
import interceptorsSetup from './providers/interceptor'
import App from './App.vue'
import 'bootstrap/dist/css/bootstrap.min.css'
/*import 'mdbvue/lib/css/mdb.min.css'*/
import './assets/scss/app.scss';
import vuetify from './plugins/vuetify';

interceptorsSetup();

new Vue({
  render: h => h(App),
  router,
  vuetify,
  store
}).$mount('#app')